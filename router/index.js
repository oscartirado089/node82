import express from 'express';
import json from 'body-parser';

export const router = express.Router();


//declarar primer ruta por omision
router.get('/',(req,res)=>{
    //res.send("<h1> Hola mundo : Oscar Tirado</h1>");
    res.render('index',{titulo:"Mis Practicas js",nombre:"Oscar Tirado"})
})

router.get('/tabla',(req,res)=>{
    //parametros
    const params ={
        numero:req.query.numero
    }
    res.render('tabla', params);
})


router.post('/tabla',(req,res)=>{
    //parametros
    const params ={
        numero:req.body.numero
    }
    res.render('tabla', params);
})




export default {router}