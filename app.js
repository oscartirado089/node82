import express from 'express';
import { fileURLToPath } from 'url';
import json from 'body-parser';
import path from 'path';
import misRutas from './router/index.js';

const main = express();
main.set("view engine", "ejs");

const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);

main.use(express.static(path.join(__dirname, 'public')));
main.use(json.urlencoded({ extended: true }));
main.use(misRutas.router);

const puerto = 80;

main.listen(puerto, () => {
    console.log("Se inició el servidor por el puerto: " + puerto);
});
